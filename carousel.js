$(document).ready(function(){
    $(".carousel_wrapper").each(function(){ initCarusel($(this)); });
})

function initCarusel(elem)
{
    $(elem).find(".carousel li:gt(0)").css("display","none");
    $(elem).find(".carousel li").each(function(){
        $(this).closest(".carousel_wrapper").find(".pager").append("<a ref='"+$(this).index()+"'><img src='"+$(this).find("img").attr('src')+"'></a>");    
    })
    $(elem).find(".pager a").first().addClass('active');
    $(elem).find(".pager a").click(function(){
        if($(this).not('.active').length>0)
        {
            $(this).parent().find("a").removeClass('active');
            $(this).addClass('active');
            $(this).closest(".carousel_wrapper").find("li:visible").fadeOut();
            $(this).closest(".carousel_wrapper").find("li:eq("+$(this).attr('ref')+")").fadeIn();  
        }
    })
    
}
